<?php

class __Mustache_25d96b039a89f202e50de953315ef045 extends Mustache_Template
{
    private $lambdaHelper;

    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $this->lambdaHelper = new Mustache_LambdaHelper($this->mustache, $context);
        $buffer = '';

        $buffer .= $indent . '
';
        $buffer .= $indent . '<div class="d-flex h-100 position-relative align-items-center bg-light h5pmessages">
';
        $buffer .= $indent . '   <div class="position-absolute py-2 bg-secondary" style="top: 0px; left: 0px; right: 0px;">
';
        $buffer .= $indent . '      <div class="container">
';
        // 'h5picon' section
        $value = $context->find('h5picon');
        $buffer .= $this->sectionA3a30407d68f3dd8be9e0e371a62e011($context, $indent, $value);
        $buffer .= $indent . '      </div>
';
        $buffer .= $indent . '   </div>
';
        $buffer .= $indent . '   <div class="container mt-5">
';
        // 'exception' section
        $value = $context->find('exception');
        $buffer .= $this->section4de1dd89371f9fed44341547333a53b1($context, $indent, $value);
        // 'info' section
        $value = $context->find('info');
        $buffer .= $this->sectionA0af63b3ade437cf0f3421c550dc3ad5($context, $indent, $value);
        // 'error' section
        $value = $context->find('error');
        $buffer .= $this->section55a458cac7a5ec2aac223a3b6e2f8e2f($context, $indent, $value);
        $buffer .= $indent . '   </div>
';
        $buffer .= $indent . '</div>
';
        // 'js' section
        $value = $context->find('js');
        $buffer .= $this->section480336643f54b80093a8f937270751ee($context, $indent, $value);

        return $buffer;
    }

    private function sectionAc6141f02a9229d7fcc9865bd9fa8116(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = 'h5p, core_h5p';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= 'h5p, core_h5p';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionA3a30407d68f3dd8be9e0e371a62e011(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
         <img src="{{{.}}}" class="h5picon" alt="{{#str}}h5p, core_h5p{{/str}}" style="width: 50px; height: auto; opacity: 0.5">
        ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '         <img src="';
                $value = $this->resolveValue($context->last(), $context);
                $buffer .= $value;
                $buffer .= '" class="h5picon" alt="';
                // 'str' section
                $value = $context->find('str');
                $buffer .= $this->sectionAc6141f02a9229d7fcc9865bd9fa8116($context, $indent, $value);
                $buffer .= '" style="width: 50px; height: auto; opacity: 0.5">
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section4de1dd89371f9fed44341547333a53b1(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
      <div class="alert alert-block fade in alert-danger my-2" role="alert">
         {{{ exception }}}
      </div>
      ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '      <div class="alert alert-block fade in alert-danger my-2" role="alert">
';
                $buffer .= $indent . '         ';
                $value = $this->resolveValue($context->find('exception'), $context);
                $buffer .= $value;
                $buffer .= '
';
                $buffer .= $indent . '      </div>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function sectionA0af63b3ade437cf0f3421c550dc3ad5(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
      <div class="alert alert-block fade in alert-info my-2" role="alert">
         {{{ . }}}
      </div>
      ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '      <div class="alert alert-block fade in alert-info my-2" role="alert">
';
                $buffer .= $indent . '         ';
                $value = $this->resolveValue($context->last(), $context);
                $buffer .= $value;
                $buffer .= '
';
                $buffer .= $indent . '      </div>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section39f696ca46f6b04bb88770888888ce5f(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '{{{code}}} : ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $value = $this->resolveValue($context->find('code'), $context);
                $buffer .= $value;
                $buffer .= ' : ';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section55a458cac7a5ec2aac223a3b6e2f8e2f(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
      <div class="alert alert-block fade in alert-warning my-2" role="alert">
         {{#code}}{{{code}}} : {{/code}}{{{ message }}}
      </div>
      ';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '      <div class="alert alert-block fade in alert-warning my-2" role="alert">
';
                $buffer .= $indent . '         ';
                // 'code' section
                $value = $context->find('code');
                $buffer .= $this->section39f696ca46f6b04bb88770888888ce5f($context, $indent, $value);
                $value = $this->resolveValue($context->find('message'), $context);
                $buffer .= $value;
                $buffer .= '
';
                $buffer .= $indent . '      </div>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

    private function section480336643f54b80093a8f937270751ee(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
(function() {
   H5PEmbedCommunicator.send(\'resize\', {
      scrollHeight: document.body.scrollHeight
   });
})();
';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '(function() {
';
                $buffer .= $indent . '   H5PEmbedCommunicator.send(\'resize\', {
';
                $buffer .= $indent . '      scrollHeight: document.body.scrollHeight
';
                $buffer .= $indent . '   });
';
                $buffer .= $indent . '})();
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

}
