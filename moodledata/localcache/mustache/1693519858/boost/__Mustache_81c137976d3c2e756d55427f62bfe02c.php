<?php

class __Mustache_81c137976d3c2e756d55427f62bfe02c extends Mustache_Template
{
    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $buffer = '';

        $buffer .= $indent . '<div class="core_contentbank_viewcontent">
';
        $buffer .= $indent . '    <div class="d-flex justify-content-end flex-column flex-sm-row">
';
        if ($partial = $this->mustache->loadPartial('core_contentbank/viewcontent/toolbarview')) {
            $buffer .= $partial->renderInternal($context, $indent . '        ');
        }
        $buffer .= $indent . '    </div>
';
        $buffer .= $indent . '    <div class="container mt-1 mb-1" data-region="viewcontent-content">
';
        $buffer .= $indent . '        ';
        $value = $this->resolveValue($context->find('contenthtml'), $context);
        $buffer .= $value;
        $buffer .= '
';
        $buffer .= $indent . '    </div>
';
        $buffer .= $indent . '    <div class="d-flex justify-content-end flex-column flex-sm-row">
';
        if ($partial = $this->mustache->loadPartial('core_contentbank/viewcontent/toolbarview')) {
            $buffer .= $partial->renderInternal($context, $indent . '        ');
        }
        $buffer .= $indent . '    </div>
';
        $buffer .= $indent . '</div>
';

        return $buffer;
    }
}
