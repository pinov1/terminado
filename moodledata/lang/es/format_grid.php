<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Strings for component 'format_grid', language 'es', version '3.11'.
 *
 * @package     format_grid
 * @category    string
 * @copyright   1999 Martin Dougiamas and contributors
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['addsection'] = 'Añadir sección';
$string['cannotconvertuploadedimagetodisplayedimage'] = 'No se puede convertir la imagen cargada en una imagen visualizada.  Informe al desarrollador de los detalles del error y de la información que se recoge en el archivo php.log.';
$string['cannotfinduploadedimage'] = 'No se puede encontrar la imagen original cargada.  Informe al desarrollador de los detalles del error y de la información que se recoge en el archivo php.log.  Actualice la página y cargue una copia nueva de la imagen.';
$string['centre'] = 'Centro';
$string['closeshadebox'] = 'Cerrar caja sombreada';
$string['colourrule'] = 'Por favor, escribe un color RGB cálido, con seis dígitos hexadecimales.';
$string['crop'] = 'Recortar';
$string['currentsection'] = 'Esta sección';
$string['default'] = 'Predeterminado - {$a}';
$string['defaultbordercolour'] = 'Color predeterminado del borde del contenedor de la imagen';
$string['defaultbordercolour_desc'] = 'El color predeterminado del borde del contenedor de la imagen.';
$string['defaultborderradius'] = 'Radio predeterminado del borde';
$string['defaultborderradius_desc'] = 'El radio predeterminado del borde está activado/desactivado.';
$string['defaultborderwidth'] = 'Ancho predeterminado del borde';
$string['defaultborderwidth_desc'] = 'El ancho predeterminado del borde.';
$string['defaultcurrentselectedimagecontainercolour'] = 'Color predeterminado del contenedor de la imagen actualmente seleccionada';
$string['defaultcurrentselectedimagecontainercolour_desc'] = 'El color predeterminado del contenedor de la imagen actualmente seleccionada.';
$string['defaultcurrentselectedsectioncolour'] = 'Color predeterminado de la sección actualmente seleccionada';
$string['defaultcurrentselectedsectioncolour_desc'] = 'El color predeterminado de la sección actualmente seleccionada.';
$string['defaultdisplayedimagefiletype'] = 'Tipo de imagen mostrada';
$string['defaultdisplayedimagefiletype_desc'] = 'Configurar el tipo de imagen mostrada';
$string['defaultfitsectioncontainertowindow'] = 'Ajustar el contenedor de la sección a la ventana de forma predeterminada';
$string['defaultfitsectioncontainertowindow_desc'] = 'El ajuste predeterminado para la opción «Ajustar el contenedor de la sección a la ventana».';
$string['defaultimagecontainerbackgroundcolour'] = 'Color de fondo predeterminado del contenedor de la imagen';
$string['defaultimagecontainerbackgroundcolour_desc'] = 'El color de fondo predeterminado del contenedor de la imagen.';
$string['defaultimagecontainerratio'] = 'Proporción predeterminada del contenedor de la imagen con respecto al ancho';
$string['defaultimagecontainerratio_desc'] = 'La proporción predeterminada del contenedor de la imagen con respecto al ancho.';
$string['defaultimagecontainerwidth'] = 'Ancho predeterminado del contenedor de la imagen';
$string['defaultimagecontainerwidth_desc'] = 'El ancho predeterminado del contenedor de la imagen.';
$string['defaultimageresizemethod'] = 'Método predeterminado de cambio de tamaño de la imagen';
$string['defaultimageresizemethod_desc'] = 'El método predeterminado para cambiar la imagen y ajustarla al contenedor.';
$string['defaultnewactivity'] = 'Mostrar imagen de notificación de nueva actividad predeterminada';
$string['defaultnewactivity_desc'] = 'Mostrar la imagen de notificación de la nueva actividad cuando se añada una nueva actividad o recurso a un valor predeterminado de la sección.';
$string['deleteimage'] = 'Borrar imagen';
$string['deleteimage_help'] = 'Elimina la imagen de la sección que se está editando.  En caso de que haya cargado una imagen, no sustituirá a la imagen eliminada.';
$string['deletesection'] = 'Borrar sección';
$string['editimage'] = 'Cambiar imagen';
$string['editimage_alt'] = 'Añadir o cambiar imagen';
$string['editsection'] = 'Editar sección';
$string['editsectionname'] = 'Editar el nombre de la sección';
$string['formatnotsupported'] = 'El formato no está soportado por este servidor; por favor repare la configuración del sistema para que tenga la extensión PHP GD instalada - {$a}.';
$string['functionfailed'] = 'Falló función en imagen - {$a}.';
$string['gfreset'] = 'Opciones de restablecimiento de rejilla';
$string['gfreset_help'] = 'Restablece los valores predeterminados de la rejilla';
$string['grid:changeimagecontainersize'] = 'Cambiar o restablecer el tamaño del contenedor de la imagen';
$string['grid:changeimagecontainerstyle'] = 'Cambiar o restablecer el estilo del contenedor de la imagen';
$string['grid:changeimageresizemethod'] = 'Cambiar o restablecer el método de cambio de tamaño de la imagen';
$string['gridimagecontainer'] = 'Imágenes en rejilla';
$string['hidden_topic'] = 'Esta sección ha sido ocultada';
$string['hidefromothers'] = 'Ocultar sección';
$string['imagecannotbeused'] = 'No se puede utilizar la imagen, debe estar en PNG, JPG o GIF, y deberá estar instalada la extensión GD PHP.';
$string['imagefile'] = 'Cargar una imagen';
$string['imagefile_help'] = 'Carga una imagen de tipo PNG, JPG o GIF.';
$string['information'] = 'Información';
$string['informationsettings'] = 'Configuraciones de Información';
$string['informationsettingsdesc'] = 'Información del formato Rejilla';
$string['left'] = 'Izquierda';
$string['markedthissection'] = 'Esta sección está destacada como sección actual';
$string['markthissection'] = 'Destacar esta sección como sección actual';
$string['mimetypenotsupported'] = 'El tipo MIME no está soportado como un formato de imagen en el formato Rejilla - {$a}.';
$string['newsectionname'] = 'Nuevo nombre para la sección {$a}';
$string['nextsection'] = 'Siguiente sección';
$string['noimageinformation'] = 'La información de la imagen está vacía - {$a}.';
$string['numbersections'] = 'Número de secciones';
$string['off'] = 'Desactivado';
$string['on'] = 'Activado';
$string['original'] = 'Original';
$string['originalheightempty'] = 'La altura original está vacía - {$a}.';
$string['originalwidthempty'] = 'El ancho original está vacío - {$a}.';
$string['page-course-view-grid'] = 'Cualquier página principal del curso en el formato rejilla';
$string['page-course-view-grid-x'] = 'Cualquier página del curso en el formato rejilla';
$string['pluginname'] = 'Formato rejilla';
$string['previoussection'] = 'Sección anterior';
$string['privacy:nop'] = 'El formato Rejilla almacena muchas configuraciones que tienen que ver con  su configuración. Ninguno de estos ajustes está relacionado con un usuario específico, Es responsabilidad de Usted asegurarse de que no se ingrese ningún dato del usuario en alguno de los campos de texto libre. El ajustar una configuración resultará en que dicha configuración se apunte en bitácora dentro del sistema de bitácoras de Moodle contra el usuario que la cambió, lo cual está fuera del control  del tema. Por favor, vea el sistema central de bitácoras para cumplimiento de privacidad al respecto. Al subir imágenes, Usted debe evitar subir imágenes con datos incrustados de ubicación (EXIF GPS) incluidos o algún otro dato personal. Sería posible extraer cualquier dato personal o de ubicación de las imágenes. Por favor examine el código cuidadosamente para asegurarse de que cumple con su interpretación de sus leyes de privacidad. Yo (el autor del plugin) no soy abogado y mi análisis está basado en mi interpretación. Si Usted tiene alguna duda entonces quite el formato inmediatamente.';
$string['reporterror'] = 'Por favor reporte los detalles del error y la información contenida en el archivo php.log al desarrollador.';
$string['resetallfitpopup'] = 'Ajustar elementos emergentes de la sección a la ventana';
$string['resetallfitpopup_help'] = 'Restablece los valores predeterminados de la opción «Ajustar elementos emergentes de la sección a la ventana» de todos los cursos para que sean los mismos que los valores iniciales de un curso en formato rejilla.';
$string['resetallgrp'] = 'Resetear todo:';
$string['resetallimagecontainersize'] = 'Tamaños de contenedor de imagen';
$string['resetallimagecontainersize_help'] = 'Restablece los valores predeterminados de los tamaños del contenedor de la imagen en todos los cursos para que sean los mismos que los valores iniciales de un curso en formato rejilla.';
$string['resetallimagecontainerstyle'] = 'Estilos de contenedor de imagen';
$string['resetallimagecontainerstyle_help'] = 'Restablece los valores predeterminados de los estilos del contenedor de la imagen en todos los cursos para que sean los mismos que los valores iniciales de un curso en formato rejilla.';
$string['resetallimageresizemethod'] = 'Métodos de cambio de tamaño de la imagen';
$string['resetallimageresizemethod_help'] = 'Restablece los valores predeterminados de los métodos de cambio de tamaño de la imagen en todos los cursos para que sean los mismos que los valores iniciales de un curso en formato rejilla.';
$string['resetallnewactivity'] = 'Nuevas actividades';
$string['resetallnewactivity_help'] = 'Restablece los valores predeterminados de las imágenes de notificación de una nueva actividad en todos los cursos para que sean los mismos que los valores iniciales de un curso en formato rejilla.';
$string['resetfitpopup'] = 'Ajustar elemento emergente de la sección a la ventana';
$string['resetfitpopup_help'] = 'Restablece los valores predeterminados de la opción «Ajustar elemento emergente de la sección a la ventana» para que sean los mismos que los valores iniciales de un curso en formato rejilla.';
$string['resetgrp'] = 'Resetear:';
$string['resetimagecontainersize'] = 'Tamaño del contenedor de la imagen';
$string['resetimagecontainersize_help'] = 'Restablece los valores predeterminados del tamaño del contenedor de la imagen para que sean los mismos que los valores iniciales de un curso en formato rejilla.';
$string['resetimagecontainerstyle'] = 'Estilo del contenedor de la imagen';
$string['resetimagecontainerstyle_help'] = 'Restablece el valor predeterminado del estilo del contenedor de la imagen para que sea el mismo que el valor inicial de un curso en formato rejilla.';
$string['resetimageresizemethod'] = 'Método de cambio de tamaño de la imagen';
$string['resetimageresizemethod_help'] = 'Restablece el valor predeterminado del método de cambio de tamaño de la imagen para que sea el mismo que el valor inicial de un curso en formato rejilla.';
$string['resetnewactivity'] = 'Nueva actividad';
$string['resetnewactivity_help'] = 'Restablece los valores predeterminados de la imagen de notificación de nueva actividad para que sean los mismos que los valores iniciales de un curso en formato rejilla.';
$string['right'] = 'Derecha';
$string['scale'] = 'Escala';
$string['section0name'] = 'General';
$string['sectionname'] = 'Sección';
$string['setbordercolour'] = 'Establecer el color del borde';
$string['setbordercolour_help'] = 'Establece el color RGB hexadecimal del borde.';
$string['setborderradius'] = 'Activar/Desactivar el radio del borde';
$string['setborderradius_help'] = 'Establece el radio del borde como activado o desactivado.';
$string['setborderwidth'] = 'Establecer el ancho del borde';
$string['setborderwidth_help'] = 'Establece el ancho del borde entre 1 y 10.';
$string['setcurrentselectedimagecontainercolour'] = 'Establecer el color del contenedor de la imagen actualmente seleccionada';
$string['setcurrentselectedimagecontainercolour_help'] = 'Establece el color RGB hexadecimal para el contenedor de la imagen actualmente seleccionada.';
$string['setcurrentselectedsectioncolour'] = 'Establecer el color de la sección actualmente seleccionada';
$string['setcurrentselectedsectioncolour_help'] = 'Establece el color RGB hexadecimal de la sección actualmente seleccionada.';
$string['setfitsectioncontainertowindow'] = 'Ajustar el elemento emergente de la sección a la ventana';
$string['setfitsectioncontainertowindow_help'] = 'Cuando esté habilitado, el cuadro emergente con el contenido de la sección se ajustará al tamaño de la ventana y se desplazará hacia el interior de ser necesario.  Si está deshabilitado, en su lugar se desplazará la página entera.';
$string['setimagecontainerbackgroundcolour'] = 'Establecer el color de fondo del contenedor de la imagen';
$string['setimagecontainerbackgroundcolour_help'] = 'Establece el color RGB hexadecimal para el fondo del contenedor de la imagen.';
$string['setimagecontainerratio'] = 'Establecer la proporción del contenedor de la imagen con respecto al ancho';
$string['setimagecontainerratio_help'] = 'Establece la proporción del contenedor de la imagen a una de: 3-2, 3-1, 3-3, 2-3, 1-3, 4-3 o 3-4.';
$string['setimagecontainerwidth'] = 'Establecer el ancho del contenedor de la imagen';
$string['setimagecontainerwidth_help'] = 'Establece el ancho del contenedor de la imagen a uno de: 128, 192, 210, 256, 320, 384, 448, 512, 576, 640, 704 o 768';
$string['setimageresizemethod'] = 'Establecer el método de cambio de tamaño de la imagen';
$string['setimageresizemethod_help'] = 'Establece el método de cambio de tamaño de la imagen a: «Escala» o «Recortar» al cambiar la imagen para ajustarla al contenedor.';
$string['setnewactivity'] = 'Mostrar imagen de notificación de nueva actividad';
$string['setnewactivity_help'] = 'Muestra la imagen de notificación de la nueva actividad cuando se añada una nueva actividad o recurso a una sección.';
$string['settings'] = 'Configuraciones';
$string['settingssettings'] = 'Configuraciones de Configuraciones';
$string['settingssettingsdesc'] = 'Configuraciones del formato Rejilla';
$string['shadeboxcontent'] = 'Contenido de la caja sombreada';
$string['showfromothers'] = 'Mostrar sección';
$string['topic'] = 'Sección';
$string['topic0'] = 'General';
$string['versionalpha'] = 'Versión alfa - Casi seguramente contiene errores. ¡Esta es una versión en desarrollo solamente para desarrolladores! ¡Ni siquiera se le ocurra instalarlo en un servidor de producción!';
$string['versionbeta'] = 'Versión beta- Probablemente contenga errores. Listo para probarse por un administrador solamente en un servidor de pruebas.';
$string['versionrc'] = 'Versión candidato a liberación- Podría contener errores. Pruébelo completamente en un servidor de pruebas antes de considerarlo para un servidor de producción.';
$string['versionstable'] = 'Versión estable - Podría contener errores. Pruébelo en un servidor de pruebas antes de instalarlo en su servidor de producción.';
$string['webp'] = 'WebP';
