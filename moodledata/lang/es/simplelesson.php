<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Strings for component 'simplelesson', language 'es', version '3.11'.
 *
 * @package     simplelesson
 * @category    string
 * @copyright   1999 Martin Dougiamas and contributors
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['action'] = 'Acción';
$string['adaptive'] = 'Retroalimentación adaptativa';
$string['adaptivenopenalty'] = 'Adaptativo (sin castigo)';
$string['add_page'] = 'Añadir página';
$string['add_question'] = 'Añadir pregunta';
$string['allocate_mark'] = 'Asignar un puntaje para el ensayo';
$string['allowreview'] = 'Permitirle al estudiante revisar intentos';
$string['allowreview_help'] = 'Al final del intento, los estudiantes pueden revisar sus respuestas y sus puntajes por defecto. Desactive esta casilla para impedirles la revisión a los estudiantes.';
$string['answer_report'] = 'Respuestas del usuario';
$string['answerquestions'] = 'Por favor conteste todas las preguntas';
$string['attempt'] = 'Intentar lección';
$string['attempt_deleted'] = 'Intento eliminado';
$string['attempt_not_deleted'] = 'Intento para eliminar registro falló';
$string['attempt_report'] = 'Intentos del usuario';
$string['attemptid'] = 'Intento';
$string['attemptstarted'] = 'Intento de lección simple iniciado';
$string['autosequencelink'] = 'Auto-secuenciar páginas';
$string['basic_report'] = 'Reporte básico';
$string['behaviour'] = 'Comportamiento de pregunta';
$string['category_select'] = 'Sleccionar categoría';
$string['categoryid'] = 'Categoría';
$string['clean_up_usages'] = 'Borrar usos de pregunta antigua para Lección simple';
$string['completequestion'] = 'Por favor, intente esta pregunta para continuar';
$string['date'] = 'Fecha del intento';
$string['deferredfeedback'] = 'Retroalimentación diferida';
$string['delete'] = 'eliminar';
$string['deleteallattempts'] = 'Eliminar todos los intentos.';
$string['deleteallsubmissions'] = 'Eliminar todos los envíos';
$string['edit_page'] = 'Editar página';
$string['edit_page_form'] = 'Editar los contenidos de la página';
$string['editing'] = 'Gestionar lección:';
$string['enablereports'] = 'Mostrar pestañas de reportes';
$string['enablereports_desc'] = 'Activar para permitirle a profesores ver reportes';
$string['end_lesson'] = 'Salir de lección';
$string['essay_grading'] = 'Calificar un ensayo';
$string['essay_grading_page'] = 'Usar esta página para calificar manualmente un envío de ensayo.';
$string['essaydate'] = 'Fecha de envío: {$a}';
$string['exportpagelink'] = 'Exportar páginas';
$string['finishreview'] = 'Terminar revisión';
$string['firstname'] = 'Nombre';
$string['firstpagelink'] = 'Primera página';
$string['gotoaddpage'] = 'Añadir página';
$string['gotodeletepage'] = 'Eliminar página';
$string['gotoeditpage'] = 'Editar página';
$string['gotosummary'] = 'Página del resumen';
$string['grade_saved'] = 'Calificación guardada';
$string['gradeaverage'] = 'Promedio de intentos';
$string['graded'] = 'Calificada';
$string['gradehighest'] = 'Intento más alto';
$string['gradelast'] = 'Último intento';
$string['gradelink'] = 'Calificar ensayo';
$string['gradelinkheader'] = 'Acción';
$string['gradelowest'] = 'Intento más bajo';
$string['grademethod'] = 'Calificación de intentos';
$string['grademethod_help'] = 'Si se hacen intentos múltiples, esta configuración determina como será calculada la calificación para el Libro de calificaciones.';
$string['immediatecbm'] = 'Retroalimentación inmediata con PBC';
$string['immediatefeedback'] = 'Retroalimentación inmediata';
$string['importpagelink'] = 'Importar páginas';
$string['lastname'] = 'Apellido(s)';
$string['lessonname'] = 'Lección';
$string['manage_attempts'] = 'Gestionar intentos';
$string['manage_pages'] = 'Gestionar páginas';
$string['manage_questions'] = 'Gestionar preguntas';
$string['manual_grade'] = 'Calificación manual';
$string['mark'] = 'Puntaje';
$string['max_attempts_exceeded'] = 'No se permiten más intentos';
$string['maxattempts'] = 'Máx intentos';
$string['maxmark'] = 'Puntos disponibles: {$a}';
$string['maxscore'] = 'De un total de';
$string['moduleid'] = 'id';
$string['modulename'] = 'Lección simple';
$string['modulenameplural'] = 'Lecciones simples';
$string['move_down'] = 'Mover hacia abajo';
$string['move_up'] = 'Mover hacia arriba';
$string['navigation'] = 'Navegación:';
$string['nextpage'] = 'Página siguiente';
$string['no_manual_grades'] = 'Nada para calificar';
$string['no_questions'] = 'No hay preguntas para intentar (usar vista previa)';
$string['numattempts'] = 'Intentos hechos: {$a} de';
$string['outof'] = 'De un total de';
$string['page_adding'] = 'Añadir una nueva página';
$string['page_deleted'] = 'Página eliminada';
$string['page_index_header'] = 'Índice';
$string['page_saved'] = 'Página guardada';
$string['page_updated'] = 'Página actualizada';
$string['pageviewed'] = 'Página de lección simple vista';
$string['pluginname'] = 'Lección simple';
$string['preview'] = 'Vista previa';
$string['preview_completed'] = 'vista previa completada';
$string['prevpage'] = 'Página anterior';
$string['privacy:metadata:simplelesson_answers:mark'] = 'El puntaje del usuario que contesta la pregunta.';
$string['privacy:metadata:simplelesson_answers:youranswer'] = 'La respuesta del usuario a la pregunta';
$string['privacy:metadata:simplelesson_attempts:sessionscore'] = 'El puntaje logrado en el intento.';
$string['privacy:metadata:simplelesson_attempts:status'] = 'El estado de finalización del intento.';
$string['privacy:metadata:simplelesson_attempts:timetaken'] = 'El tiempo empleado para completar el intento (segundos).';
$string['privacy:metadata:simplelesson_attempts:userid'] = 'La id del usuario que hace el intento.';
$string['qnumber'] = 'Pregunta número';
$string['question'] = 'Pregunta';
$string['question_editing'] = 'Editando preguntas';
$string['question_name'] = 'Nombre de la pregunta';
$string['question_text'] = 'Texto de la pregunta';
$string['questions_added'] = 'Preguntas añadidas';
$string['questionscore'] = 'Puntaje';
$string['questionsummary'] = 'Pregunta';
$string['reportstab'] = 'reportes';
$string['requires_grading'] = 'Necesita calificación';
$string['rightanswer'] = 'Respuesta correcta';
$string['sequence_updated'] = 'Secuencias de página actualizadas';
$string['sessionscore'] = 'Correcto';
$string['setpage'] = 'Asignar';
$string['showindex'] = 'Mostrar el índice de página';
$string['showindex_help'] = 'El índice de página es opcional y se mostrará por defecto en la parte superior derecha de todas las páginas de contenido en modo de previsualización. Esto puede ser sobrescrito por los temas.';
$string['showpage'] = 'Previsualizar página';
$string['simplelesson:addinstance'] = 'Añadir una nueva Lección simple';
$string['simplelesson:exportreportpages'] = 'Exportar páginas de reportes';
$string['simplelesson:manage'] = 'Gestionar Lección simple';
$string['simplelesson:manageattempts'] = 'Gestionar registros de intentos';
$string['simplelesson:view'] = 'Ver Lección simple';
$string['simplelesson:viewreportstab'] = 'Ver la pestaña de reportes';
$string['simplelesson_editing'] = 'Editando Lección simple';
$string['simplelesson_settings'] = 'Configuraciones de Lecciónsimple';
$string['simplelesson_title'] = 'Título de este recurso';
$string['simplelessonfieldset'] = 'Conjunto_de_campo de ejemplo personalizado';
$string['simplelessonname'] = 'Nombre de Lección simple';
$string['simplelessonname_help'] = 'Elegir un nombre apropiado para su Lección simple.';
$string['simplelessonviewed'] = 'Lección simple vista';
$string['starting_attempt'] = 'Iniciando Intento';
$string['status'] = 'Estado';
$string['summary_header'] = 'Resumen del estado';
$string['summary_score'] = 'Puntaje para este intento: {$a}';
$string['summary_time'] = 'Tiempo total para este intento: {$a} (Segundos)';
$string['summary_user'] = 'Reporte del usuario para {$a}';
$string['timecreated'] = 'Hora de creación';
$string['ungraded'] = 'No calificada';
$string['unlimited'] = 'Ilimitados';
$string['unlimited_attempts'] = 'Intentos ilimitados';
$string['userdetail'] = 'Usuario: {$a}';
$string['userreportdownload'] = 'Descargar reporte del usuario (CSV)';
$string['viewtab'] = 'ver';
$string['youranswer'] = 'Su respuesta';
